﻿using Booklist_MVC.Interfaces;
using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Booklist_MVC.Services
{
    public class GetAuthorization : IGetAuthorization
    {
        public async Task<TokenResponse> GetTokenFromIdentityServer()
        {
            var client = new HttpClient();
            var serverResp = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (serverResp.IsError)
            {
                var error = serverResp.Error;
                //throw new System.ArgumentException(error);
            }
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = serverResp.TokenEndpoint,
                ClientId = "clientBookListMVC",
                ClientSecret = "secret",
                Scope = "apiBookListMVC"
            });
            return tokenResponse;
        }

        public async Task<HttpResponseMessage> GetAuthorizationFromAPI(TokenResponse tokenResponse)
        {
            var apiClient = new HttpClient();
            apiClient.SetBearerToken(tokenResponse.AccessToken);
            var response = await apiClient.GetAsync("https://localhost:44340/api/identity");
            return response;
        }
    }
}
