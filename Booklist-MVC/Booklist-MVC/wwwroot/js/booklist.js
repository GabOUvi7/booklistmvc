﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
})

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "/books/getall/",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "name", "width": "30%" },
            { "data": "author", "width": "30%" },
            { "data": "isbn", "width": "30%" },
            {
                "data": "id", "render": function (data) {
                    return `<div class="text-center">
                               <button class="btn btn-danger text-white" onclick=Delete('/books/Delete?id='+${data})>Delete</button>
                            </div>`
                }, "width": "30%"
            },
            {
                "data": "id", "render": function (data) {
                    return `<div class="text-center">
                               <a href="/books/Upsert?id=${data}" class="btn btn-warning">Edit</a>
                            </div>`
                }, "width": "30%"
            }
        ],
        "langueage": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    })
}

function Delete(url) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover",
        icon: "warning",
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    } else {
                        toastr.success(data.message);
                    }
                }
            });
        }
    });
}