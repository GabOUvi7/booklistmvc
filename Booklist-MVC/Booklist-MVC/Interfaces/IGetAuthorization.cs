﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Booklist_MVC.Interfaces
{
    public interface IGetAuthorization
    {
        Task<TokenResponse> GetTokenFromIdentityServer();
        Task<HttpResponseMessage> GetAuthorizationFromAPI(TokenResponse tokenResponse);
    }
}
