﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booklist_MVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using Booklist_MVC.Interfaces;

namespace Booklist_MVC.Controllers
{
    [Authorize]
    public class BooksController : Controller
    {

        private readonly DB _db;
        private readonly IGetAuthorization _getAuthorization;

        [BindProperty]
        public Book Book { get; set; }
        public BooksController(DB db, IGetAuthorization getAuthorization)
        {
            _db = db;
            _getAuthorization = getAuthorization;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var token = await _getAuthorization.GetTokenFromIdentityServer();
                var response = await _getAuthorization.GetAuthorizationFromAPI(token);
                if (!response.IsSuccessStatusCode)
                {
                    throw new System.ArgumentException(response.StatusCode.ToString());
                }
                var content = await response.Content.ReadAsStringAsync();
                return View();
            }
            catch
            {
                Response.Redirect("/Account/Logout");
                return Redirect("/");
            }

        }

        public IActionResult Upsert(int? id)
        {
            Book = new Book();
            if (id == null)
            {
                //create book
                return View(Book);
            }
            //update book
            Book = _db.Books.FirstOrDefault(u => u.Id == id);
            if (Book == null)
            {
                return NotFound();
            }
            return View(Book);
        }

        #region APICalls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Json(new { data = await _db.Books.ToListAsync() });
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var bookFromDB = await _db.Books.FindAsync(id);
            if (bookFromDB == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _db.Books.Remove(bookFromDB);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Delete successful!" });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert()
        {
            if (ModelState.IsValid)
            {
                if (Book.Id == 0)
                {
                    _db.Books.Add(Book);
                }
                else
                {
                    _db.Books.Update(Book);
                }
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Book);
        }
        #endregion
    }
}
